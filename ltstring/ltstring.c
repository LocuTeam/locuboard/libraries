/*
 * ltstring library © 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ltstring.c

  @Version
    00.03

  @Summary
    Library for easier string using.
*/

#include "ltstring.h"

#include <assert.h>


uint32_t get_char_index_in_string(const char *string, const char c, const uint16_t len){
    for(uint16_t i = 0; i < len; ++i){
        if(string[i] == c) return i;
    }
    return (-1);
}

inline uint8_t char_to_number(const char c){
    return ((uint8_t)(c - 48));
}

inline uint8_t hexchar_to_number(const char c){
    if((c > 47) && (c < 58)){ // '0' - '9'
        return ((uint8_t)(c - 48));
    }

    if((c > 64) && (c < 71)){ // 'A' - 'F'
        return ((uint8_t)(c - 55));
    }

    if((c > 96) && (c < 103)){ // 'a' - 'f'
        return ((uint8_t)(c - 87));
    }

    return (-1); // Error (-1)
}

inline char number_to_char(const uint8_t number){
    return ((char)(number + 48));
}

inline char number_to_hexchar(const uint8_t number, const HEXNUMBER_CASE_OPTION_e case_option){
    if((number >= 0) && (number < 10)){
        return number_to_char(number);
    }
    if((number > 9) && (number < 16)){
        if(case_option == HEXNUMBER_CASE_OPTION_LOWER){
            return ((char)(number + 87));
        }
        //else
        return ((char)(number + 55));
    }
    
    return LTSTRING_NULCHAR;
}

inline char *slice_string_p(char *string, const uint16_t size, const uint16_t start, const uint16_t end){
    if(start >= size || start > end || end >= size) return 0; // NULL
    string[end] = '\0';
    return ((char *)(string + start));
}

char *slice_string(char *string, const uint16_t size, const uint16_t start, const uint16_t end){
    if(start >= size || start > end || end >= size) return 0; // NULL
    for(uint16_t i = start; i < end; i++){
        string[i - start] = string[i];
    }
    string[end - start] = '\0';
    return string;
}

inline uint8_t is_aplhachar(char c){
    return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

inline char char_to_lowercase(char c){
    if((c >= 'A' && c <= 'Z')) return (c + 32);
    return c;
}

inline char char_to_uppercase(char c){
    if((c >= 'a' && c <= 'z')) return (c - 32);
    return c;
}

int8_t compare_string(char *str1, char *str2, const uint16_t start, const uint16_t end){
    assert( start < end && str1 && str2); // Better than memory overwritting or program fall for unknown reason
    
    str1 = (str1 + start);
    str2 = (str2 + start);
    char *end_ptr = (str1 + end - 1);

    while (*str1){ // if string len does not equals and str2 is at the end the last char is '\0' -> the if statement breaks - it is valid
        if ((*str1) != (*str2)) break;
        if(str1 == end_ptr) return 0; // Reached parametr end
        ++str1;
        ++str2;
    }

    int8_t _tmp = ((*((const unsigned char*) str1)) - (*((const unsigned char*) str2)));

    return ( (_tmp > 0) ? 1 : (_tmp == 0) ? 0 : (-1) );
}

uint8_t number_to_hexstring(char *string, uint32_t number, uint8_t view_size, const HEXNUMBER_CASE_OPTION_e case_option){
    uint8_t _temp[8]; // maximum 8 hexchars
    uint8_t _temp_cnt = 0;
    uint8_t _string_cnt = 0;

    do{
        _temp[_temp_cnt] = number & 0xF;
        ++_temp_cnt;
        number = number >> 4;
    } while(number > 0);

    view_size = (view_size > 8) ? 8 : view_size;
 
    for(uint8_t i = _temp_cnt; i > 0; ++_string_cnt){
        if(view_size > _temp_cnt){
            string[_string_cnt] = '0';
            --view_size;
        }
        else{
            string[_string_cnt] = number_to_hexchar(_temp[--i], case_option);
        }
    }
    string[_string_cnt] = '\0';
    return _string_cnt;
}

uint8_t number_to_string(char *string, uint32_t number, const uint8_t base, const uint8_t view_size){
    uint8_t _temp[32]; // maximum 32 chars
    uint8_t _temp_cnt = 0;
    uint8_t _string_cnt = 0;

    do{
        _temp[_temp_cnt++] = number % base;
        number /= base;
    } while(number > 0);
    
    uint8_t _view_size = (view_size > 32) ? 32 : view_size;
 
    for(uint8_t i = _temp_cnt; i > 0; _string_cnt++){
        if(_view_size > _temp_cnt){
            string[_string_cnt] = '0';
            --_view_size;
        }
        else{
            --i;
            string[_string_cnt] = (_temp[i] >= 10)? _temp[i] + 55 : _temp[i] + 48;
        }
    }
    string[_string_cnt] = '\0';
    return _string_cnt;
}

inline const char *ltstring_get_version(void){
    return LTSTRING_LIBRARY_VERSION;
}
