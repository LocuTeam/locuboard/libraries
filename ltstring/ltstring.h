/*
 * ltstring library © 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ltstring.h

  @Version
    00.03

  @Summary
    Library for easier string using.
*/


#ifndef _LTSTRING_H    /* Guard against multiple inclusion */
#define _LTSTRING_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */

    #define LTSTRING_NULCHAR UINT8_C(0x00)
    
    #define LTSTRING_LIBRARY_VERSION "00.03"


    /* ************************************************************************** */
    /* Section: Data Types                                                        */
    /* ************************************************************************** */

    typedef enum{
        HEXNUMBER_CASE_OPTION_LOWER = 0,
        HEXNUMBER_CASE_OPTION_UPPER = 1
    } HEXNUMBER_CASE_OPTION_e;



    /* ************************************************************************** */
    /* Section: Function Prototypes                                               */
    /* ************************************************************************** */

    uint32_t get_char_index_in_string(const char *string, const char c, const uint16_t len);
    inline uint8_t char_to_number(const char c);
    inline uint8_t hexchar_to_number(const char c);
    inline char number_to_char(const uint8_t number);
    inline char hexnumber_to_char(const uint8_t number, const HEXNUMBER_CASE_OPTION_e case_option);
    inline char *slice_string_p(char *string, const uint16_t size, const uint16_t start, const uint16_t end);
    char *slice_string(char *string, const uint16_t size, const uint16_t start, const uint16_t end);
    inline uint8_t is_aplhachar(char c);
    inline char char_to_lowercase(char c);
    inline char char_to_uppercase(char c);
    int8_t compare_string(char *str1, char *str2, const uint16_t start, const uint16_t end);
    uint8_t number_to_hexstring(char *string, uint32_t number, uint8_t view_size, const HEXNUMBER_CASE_OPTION_e case_option);
    uint8_t number_to_string(char *string, uint32_t number, const uint8_t base, const uint8_t view_size);
    inline const char *ltstring_get_version(void);


/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _LTSTRING_H */
