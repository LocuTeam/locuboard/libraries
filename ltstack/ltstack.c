/*
 * ltstack library © 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ltstack.c

  @Version
    00.01

  @Summary
    Library of stack implementation for easier work.
*/


#include "ltstack.h"

uint8_t ltstack_init(ltstack_t *me, uint8_t *buffer, uint16_t capacity){
    if(!capacity || !buffer) return 0;
    me->buffer   = buffer;
    me->capacity = capacity;
    me->rw_index = 0;
    return 1;
}

inline uint8_t ltstack_get_awaiting_count(const ltstack_t *me){
    return me->rw_index;
}

inline uint8_t ltstack_is_full(const ltstack_t *me){
    return (me->rw_index == me->capacity);
}

inline uint8_t ltstack_is_empty(const ltstack_t *me){
    return (me->rw_index == 0);
}

inline uint8_t ltstack_push_value(ltstack_t *me, uint8_t value){
    if(ltstack_is_full(me)) return 0;
    me->buffer[me->rw_index] = value;
    ++(me->rw_index);
    return 1;
}

uint8_t ltstack_push_data(ltstack_t *me, uint8_t *data, uint16_t count){
    uint16_t index = 0;
    for(; !ltstack_is_full(me) && index < count; index++){
        me->buffer[me->rw_index] = data[index];
        ++(me->rw_index);
    }
    return index;
}

inline uint8_t ltstack_pop_value(ltstack_t *me){
    if(ltstack_is_empty(me)) return 0;
    uint8_t data = me->buffer[--(me->rw_index)];
    return data;
}

inline uint8_t ltstack_pop_value_p(ltstack_t *me, uint8_t *value){
    if(ltstack_is_empty(me)) return 0;
    *value = me->buffer[--(me->rw_index)];
    return 1;
}

uint8_t ltstack_pop_data(ltstack_t *me, uint8_t *data, uint16_t count){
    uint16_t index = 0;
    for(; !ltstack_is_empty(me) && index < count; index++){
        data[index] = me->buffer[--(me->rw_index)];
    }
    return index;
}

inline const char *ltstack_get_version(void){
    return LTSTACK_LIBRARY_VERSION;
}
