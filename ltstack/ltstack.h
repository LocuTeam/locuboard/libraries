/*
 * ltstack library © 2022 by Miroslav Soukup is licensed under 
 * Attribution-NonCommercial-NoDerivatives 4.0 International. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ 
*/

/*
  @File Name
    ltstack.h

  @Version
    00.01

  @Summary
    Library of stack implementation for easier work.
*/


#ifndef _LTSTACK_H    /* Guard against multiple inclusion */
#define _LTSTACK_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>



/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */

    #define LTSTACK_LIBRARY_VERSION "00.01"



    /* ************************************************************************** */
    /* Section: Data Types                                                        */
    /* ************************************************************************** */

    typedef struct ltstack_descriptor_t ltstack_t;

    struct ltstack_descriptor_t{
        uint8_t *buffer;
        volatile uint16_t capacity;
        volatile uint16_t rw_index;
    };



    /* ************************************************************************** */
    /* Section: Function Prototypes                                               */
    /* ************************************************************************** */

    uint8_t ltstack_init(ltstack_t *me, uint8_t *buffer, uint16_t capacity);
    inline uint8_t ltstack_get_awaiting_count(const ltstack_t *me);
    inline uint8_t ltstack_is_full(const ltstack_t *me);
    inline uint8_t ltstack_is_empty(const ltstack_t *me);
    inline uint8_t ltstack_push_value(ltstack_t *me, uint8_t value);
    uint8_t ltstack_push_data(ltstack_t *me, uint8_t *data, uint16_t count);
    inline uint8_t ltstack_pop_value(ltstack_t *me);
    inline uint8_t ltstack_pop_value_p(ltstack_t *me, uint8_t *value);
    uint8_t ltstack_pop_data(ltstack_t *me, uint8_t *data, uint16_t count);
    inline const char *ltstack_get_version(void);


/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _LTSTACK_H */
